# -*- coding: utf-8 -*-
"""
Created on Thu Apr 19 18:07:50 2018

@author: Vadim Frolov
"""

import pandas as pd
import os, sys
import django
import pickle
import hashlib

from dbUtils import printProgressBar, isDirectImageLink, normalizeId

sys.path.append(os.environ.get('RUNDATA_DIR'))
django.setup()
from rundatanet.runes.models import (Signature, MetaInformation, 
                                     ImageLink)

def main():
    df = pd.read_excel('C:/Program Files (x86)/Rundata/RUNDATA.xls', sheet_name='rundata')
    scrapes = pickle.load(open('scrappedSignatures.p', 'rb'))
    
    printProgressBar(0, len(df.index), prefix='Progress: ', suffix='Complete', length=50)

    linkObjs = {}
    for i in df.index:
        printProgressBar(i, len(df.index), prefix='Progress: ', suffix='Complete', length=50)
        id = df['Signum'][i]
        # convert double spaces to single
        id = id.replace("  ", " ")
        cleanId = normalizeId(id)
        
        if not Signature.objects.filter(signature_text=cleanId).exists():
            continue
        
        s = Signature.objects.get(signature_text=cleanId)
        if not s.parent_id == None:
            # this is alias, it has no meta
            continue
        
        mi = s.meta;
        
        validDomains = ['digi20.digitale-sammlungen.de', 'fmis.raa.se', 'khm.uio.no', 'www.kms.raa.se', 'www.nb.no']
        if not pd.isnull(df['Bildlänk'][i]):
            imageLinks = df['Bildlänk'][i].split(',')
            for imgLink in imageLinks:
                imgLink = imgLink.strip()
                if not any(vd in imgLink for vd in validDomains):
                    # we know that these links are invalid
                    continue
                
                key = hashlib.md5(imgLink.encode('utf-8')).hexdigest()
                # check that link is not present already
                if key in linkObjs:
                    continue;
                    
                linkResult = isDirectImageLink(imgLink)
                if linkResult == -1:
                    # link is invalid, no resource under that name
                    continue;
                    
                newImageLink = ImageLink()
                newImageLink.meta = mi
                newImageLink.link_url = imgLink
                if linkResult == 1:
                    newImageLink.direct_url = imgLink
                
                linkObjs[imgLink] = newImageLink
        
        # check if we have something in scraped data
        if scrapes[cleanId] and scrapes[cleanId]['directLinks']:
            for linkInd, directLink in enumerate(scrapes[cleanId]['directLinks']):
                key = hashlib.md5(directLink.encode('utf-8')).hexdigest()
                # check that link is not present already
                if key in linkObjs:
                    continue
                #print("Processing {}".format(cleanId))
                linkResult = isDirectImageLink(directLink)
                if linkResult == -1:
                    # link is invalid
                    continue
                
                newImageLink = ImageLink()
                newImageLink.meta = mi
                newImageLink.direct_url = directLink
                if len(scrapes[cleanId]['sourceLinks']) > linkInd:
                    newImageLink.link_url = scrapes[cleanId]['sourceLinks'][linkInd]
                else:
                    newImageLink.link_url = directLink
                
                linkObjs[key] = newImageLink
    
    pickle.dump(linkObjs, open('dumpedImageLinks.p', 'wb'))
    
    ImageLink.objects.bulk_create(list(linkObjs.values()))
        
    print('')
    print('Done')

if __name__ == "__main__":
    main()
    