"""
Script to add Swedish translation to the database
"""
import pandas as pd
import os
import sys
import django
from tqdm import tqdm

from dbUtils import normalizeId, get_search_value


def get_value(row) -> str:
    """
     We are interested in 3 first columns: Signum, BetterTranslation, OriginalTranslation ref as A, B, C.
    | B | C | Take |
    |---|---|:---:|
    | yes | yes | B |
    | yes | no | B |
    | no | yes | C |
    | no | no | null |
    """
    def get_raw_value(row):
        if row["BetterTranslation"]:
            return row["BetterTranslation"]
        elif row["OriginalTranslation"]:
            return row["OriginalTranslation"]
        return ""

    res = get_raw_value(row).strip()
    if res in ["...", "…", "¬", "°", "§A ... §B ...", ]:
        return ""
    return res


def main():
    django.setup()
    from rundatanet.runes.models import TranslationSwedish, Signature

    df = pd.read_excel("Swedish_translation_2021-12-22.xlsx")
    print(f"Got {len(df)} rows from the raw file")

    # replace NaN with empty string
    df = df.fillna("")

    # Get translation values
    df['SwedishTranslation'] = df.apply(get_value, axis=1)

    # drop empty translations
    df = df[df['SwedishTranslation'] != ""]
    print(f"Got {len(df)} rows with translation")

    objs = []
    non_existing_signatures = []
    for row in tqdm(df.itertuples(), total=len(df)):
        try:
            signature = Signature.objects.get(signature_text=normalizeId(row.Signum))
        except Signature.DoesNotExist:
            non_existing_signatures.append(row.Signum)
            continue

        search_value = get_search_value(row.SwedishTranslation)

        inst = TranslationSwedish()
        inst.value = row.SwedishTranslation
        inst.search_value = search_value
        inst.signature = signature
        objs.append(inst)

    print("List of signatures with translation but not found in the database:")
    for signum in non_existing_signatures:
        print(signum)

    TranslationSwedish.objects.bulk_create(objs)
    print("Done")


if __name__ == "__main__":
    rundata_dir = os.environ.get("RUNDATA_DIR")
    if not rundata_dir:
        print(
            "Please set the environment variable RUNDATA_DIR to the root of the rundata-net repository. Rerun the script after that.")
        sys.exit(1)
    if not os.environ.get("DJANGO_SETTINGS_MODULE"):
        print("Setting env variable DJANGO_SETTINGS_MODULE to 'config.settings.local'. Modify if needed.")
        os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.local")
    sys.path.append(rundata_dir)

    main()
