"""Add years information to the database"""
import environ
import joblib
import sys
import django
import os
from tqdm import tqdm

settings_module = "config.settings.local"
os.environ.setdefault("DJANGO_SETTINGS_MODULE", settings_module)
env = environ.Env()

sys.path.append(str(env.path('RUNDATA_DIR')))

django.setup()
from rundatanet.runes.models import (Signature, MetaInformation)


def main():
    runnor_db = joblib.load('runnor_db.joblib')
    all_meta = MetaInformation.objects.all()
    log = []
    for cur_meta in tqdm(all_meta):
        signature = cur_meta.signature.signature_text
        if signature not in runnor_db:
            log.append(f"Signature {signature} not found in runnor_db")
            continue

        cur_runnor_db = runnor_db[signature]
        if 'termini' in cur_runnor_db and cur_runnor_db['termini']:
            cur_meta.year_from = cur_runnor_db['termini']['tpq']
            cur_meta.year_to = cur_runnor_db['termini']['taq']
        cur_meta.save()

    if log:
        print("\n".join(log))


if __name__ == '__main__':
    main()