# -*- coding: utf-8 -*-
"""
This is an auxilary file that processes late aliases. Can be used if importMeta
fails on aliases (which it should not do).

@author: Vadim Frolov
"""

import pandas as pd
import os, sys
import django

from dbUtils import normalizeId, printProgressBar

sys.path.append(os.environ.get('RUNDATA_DIR'))

django.setup()
from rundatanet.runes.models import (Signature)

def main():
    df = pd.read_excel('C:/Program Files (x86)/Rundata/RUNDATA.xls', sheet_name='rundata')

    lateAliases = {}

    printProgressBar(0, len(df.index), prefix='Progress: ', suffix='Complete', length=50)

    for i in df.index:
        printProgressBar(i, len(df.index), prefix='Progress: ', suffix='Complete', length=50)
        id = df['Signum'][i]
        foundLocation = df['Plats'][i];
        # convert double spaces to single
        id = id.replace("  ", " ")
        cleanId = normalizeId(id)

        if (id.find("UTGÅR") != -1):
            print('Inscription {} is deleted. Will not add it to the DB.'.format(id))
            continue

        manualAliases = ['U 199']
        # it is actually sufficient to check ('=' in id) only
        if (pd.isnull(foundLocation) and ('=' in id)) or (cleanId in manualAliases):
            # this is an alias, cleanId - alias name, parent - parent signature
            equalIdx = id.find('=', 0)
            parent = id[equalIdx+1:].strip();
            parent = parent.replace('_', ' ');

            if not Signature.objects.filter(signature_text=cleanId).exists():
                # This signature is not in the DB yet

                gotObject = True
                parentObj = []
                try:
                    parentObj = Signature.objects.get(signature_text=parent)
                    # check that this signature is not a child itself
                    if parentObj.parent:
                        parentObj = parentObj.parent
                except Signature.DoesNotExist:
                    # do nothing
                    gotObject = False
                    
                if parentObj:
                    # add cleanId as a child of parent
                    parentObj.children.create(signature_text=cleanId)
                else:
                    # parent is not in the DB neither. We have to add this signature later
                    # after the parent.
                    lateAliases[cleanId] = parent

            # continue if this was an alias
            continue;
            
    printProgressBar(len(df.index), len(df.index), prefix='Progress: ', suffix='Complete', length=50)

    # Time to process late aliases
    print("Got {} late aliases".format(len(lateAliases.items())))
    for key, value in lateAliases.items():
        # key - clean Id (i.e. id that is not in the DB), value - parent Id (parent of key)
        
        if Signature.objects.filter(signature_text=value).exists():
            parentObj = Signature.objects.get(signature_text=value)
            
            # check that this signature is not a child itself
            if parentObj.parent:
                parentObj = parentObj.parent
            parentObj.children.create(signature_text=key)

if __name__ == "__main__":
    main()
