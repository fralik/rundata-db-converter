# -*- coding: utf-8 -*-
"""
Extract coordinates and image information from http://runinskrifter.net/
Save extracted info as a dictionary in a pickled file scrappedSignatures.p.

@author: Vadim Frolov
"""
import pandas as pd
from lxml import html
import requests
from pyproj import Proj, transform
import pickle

from dbUtils import printProgressBar, normalizeId

def scrapSignature(cleanId):
    swerefProj = Proj(init='epsg:3006')
    wgs84 = Proj(init="epsg:4326")
    found_lat_lon = []
    current_lat_lon = []
    directLinks = []
    sourceLinks = []
    
    urlBase = 'http://www.runinskrifter.net/signum/'
    spaceInd = cleanId.find(' ')
    if spaceInd == -1:
        print('Not found space in {}'.format(cleanId))
        return {}
    
    signum1= cleanId[:spaceInd]
    signum2 = cleanId[spaceInd+1:]
    pageUrl = urlBase + signum1 + '/' + signum2
    page = requests.get(pageUrl)
    if not page.ok:
        print('Failed to load page for signature {}'.format(cleanId))
        return {}
    
    tree = html.fromstring(page.content)
    figs = tree.xpath('//figure[@class="oldest"]')
    for figure in figs:
        systemName = figure.xpath('.//figcaption/text()')[0]
        if systemName != 'WGS84/ETRS89':
            continue
        textCoordinates = figure.xpath('.//td/data/@value')
        found_lat_lon = [float(x) for x in textCoordinates]
        break
    
    figs = tree.xpath('//figure[@class="latest"]')
    for figure in figs:
        systemName = figure.xpath('.//figcaption/text()')[0]
        if systemName != 'WGS84/ETRS89':
            continue
        textCoordinates = figure.xpath('.//td/data/@value')
        current_lat_lon = [float(x) for x in textCoordinates]
        break

    imgSection = tree.xpath('//section[@id="linked-images"]')
    if not imgSection:
        return {'found_lat_lon': found_lat_lon, 'current_lat_lon': current_lat_lon,
                'directLinks': directLinks, 'sourceLinks': sourceLinks}
    
    imgSection = imgSection[0] # we only have one
    directLinks = imgSection.xpath('.//figure/a/img/@src')
    sourceLinks = imgSection.xpath('.//figure/figcaption/a[4]/@href') # source if the fourth child of figcaption
    
    return {'found_lat_lon': found_lat_lon, 'current_lat_lon': current_lat_lon,
            'directLinks': directLinks, 'sourceLinks': sourceLinks}
    
def main():
    df = pd.read_excel('C:/Program Files (x86)/Rundata/RUNDATA.xls', sheet_name='rundata')

    printProgressBar(0, len(df.index), prefix='Progress: ', suffix='Complete', length=50)
    scrapDictionary = {}
    for i in df.index:
        printProgressBar(i, len(df.index), prefix='Progress: ', suffix='Complete', length=50)
        id = df['Signum'][i]
        foundLocation = df['Plats'][i];
        # convert double spaces to single
        id = id.replace("  ", " ")
        cleanId = normalizeId(id)

        #print(id)

        manualAliases = ['U 199']
        if (pd.isnull(foundLocation) and ('=' in id)) or (cleanId in manualAliases):
            continue
        
        scrapRes = scrapSignature(cleanId)
        if scrapRes:
            scrapDictionary[cleanId] = scrapRes

    pickle.dump(scrapDictionary, open('scrappedSignatures.p', 'wb'))
        
if __name__ == "__main__":
    main()