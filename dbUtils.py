"""
Created on Mon May  7 19:05:04 2018

@author: Vadim Frolov
"""
import re
from urllib.request import urlopen

def normalizeId(id):
    firstSpace = id.find(' ')
    secondSpace = id.find(' ', firstSpace + 1)
    normalizedId = id[:secondSpace] if secondSpace != -1 else id
    normalizedId = normalizedId.strip()
    return normalizedId

def isDirectImageLink(url):
    image_formats = ("image/png", "image/jpeg", "image/gif")
    try:
        site = urlopen(url)
    except Exception:
        return -1

    meta = site.info()  # get header of the http request
    if meta["content-type"] in image_formats:  # check if the content-type is a image
        return 1
    
    return 0
    
# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total: 
        print()


def get_search_value(value):
    """ Prepare value for searching """
    all_patterns = r"[\$\[\]\"\{\}\(\)\?<>\^`´\|¬°·:×¤\+÷']|¶+"
    search_value = re.sub(all_patterns, '', value)
    search_value = re.sub(r"\s{2,}", ' ', search_value)  # we get double spaces from things like ' · '
    search_value = search_value.strip()

    return search_value
