# -*- coding: utf-8 -*-
"""
Created on Wed Mar 14 19:19:40 2018

@author: Vadim Frolov
"""

import pandas as pd
import myGeo
import os, sys
import django
import re
import pickle

from dbUtils import printProgressBar, normalizeId

sys.path.append(os.environ.get('RUNDATA_DIR'))
django.setup()
from rundatanet.runes.models import (Signature,CrossForm,CrossDefinition,Cross,MaterialType,
    Material, MetaInformation, ImageLink)

def toText(value):
    return '' if pd.isnull(value) else value

def main():
    df = pd.read_excel('C:/Program Files (x86)/Rundata/RUNDATA.xls', sheet_name='rundata')

    originalIds = []
    lateAliases = {}
    # map Swedish names to English
    materialTypesMap = {'ben/horn': 'bone/antler',
        'metall': 'metal',
        'puts': 'plaster',
        'sten': 'stone',
        'trä': 'wood',
        'övrigt': 'other',
        'okänd': 'unknown'
    }
    scrapes = pickle.load(open('scrappedSignatures.p', 'rb'))

    if not MaterialType.objects.filter(name='metal').exists():
        for key, value in materialTypesMap.items():
            MaterialType.objects.create(name=value)

    if not CrossForm.objects.filter(name='A1').exists():
        lager = {'A': 9,
                 'B': 4,
                 'C': 11,
                 'D': 6,
                 'E': 11,
                 'F': 4,
                 'G': 6}
        for key, value in lager.items():
            for x in range(0, value):
                CrossForm.objects.create(name='{}{}'.format(key, x+1),
                                         group_id=ord(key)-ord('A')+1)

    printProgressBar(0, len(df.index), prefix='Progress: ', suffix='Complete', length=50)

    manualCrossForms = ['B1 (B2?)', 'A1 (A5?)', 'A1 (A4?)', '0 (G6?)', 'D3 (D1?)', 'B1 (B3?)', 'F3 (F4?)']

    metaObjs = []
    inscrObjs = []
    print("")
    for i in df.index:
        printProgressBar(i, len(df.index), prefix='Progress: ', suffix='Complete', length=50)
        id = df['Signum'][i]
        foundLocation = df['Plats'][i];
        # convert double spaces to single
        id = id.replace("  ", " ")
        cleanId = normalizeId(id)

        manualAliases = ['U 199']
        # it is actually sufficient to check ('=' in id) only
        if (pd.isnull(foundLocation) and ('=' in id)) or (cleanId in manualAliases):
            # this is an alias, cleanId - alias name, parent - parent signature
            equalIdx = id.find('=', 0)
            parent = id[equalIdx+1:].strip();
            parent = parent.replace('_', ' ');

            if not Signature.objects.filter(signature_text=cleanId).exists():
                # This signature is not in the DB yet

                gotObject = True
                parentObj = []
                try:
                    parentObj = Signature.objects.get(signature_text=parent)
                    # check that this signature is not a child itself
                    if parentObj.parent:
                        parentObj = parentObj.parent
                except Signature.DoesNotExist:
                    # do nothing
                    gotObject = False

                if parentObj:
                    # add cleanId as a child of parent
                    parentObj.children.create(signature_text=cleanId)
                else:
                    # parent is not in the DB neither. We have to add this signature later
                    # after the parent.
                    lateAliases[cleanId] = parent

            # continue if this was an alias
            continue;

        inscr = Signature()
        inscr.signature_text = cleanId
        inscrObjs.append(inscr)

    Signature.objects.bulk_create(list(inscrObjs))

    # Time to process late aliases
    print(" ")
    numAliases = len(lateAliases.items())
    print("Got {} late aliases".format(numAliases))
    i = 0
    aliasObjs = []
    
    # We know that DR 64 late alias gives an error. Let's add it manually
    parentObj = Signature.objects.get(signature_text='DR 63')
    alias = Signature()
    alias.signature_text = 'DR 64'
    alias.parent = parentObj
    alias.save()
    lateAliases.pop('DR 64')
    
    for key, value in lateAliases.items():
        printProgressBar(i, numAliases, prefix='Progress: ', suffix='Complete', length=50)
        i = i + 1

        if value == 'DR 64':
            # DR 64 is itself an alias, let's point to a corerct inscription
            value = 'DR 63'

        # key - clean Id (i.e. id that is not in the DB), value - parent Id (parent of key)
        if not Signature.objects.filter(signature_text=value).exists():
            # The parent itself doesn't exist.
            print("No signature {}".format(value))
            continue

        parentObj = Signature.objects.get(signature_text=value)

        # check that this signature is not a child itself
        if parentObj.parent:
            parentObj = parentObj.parent
        #parentObj.children.create(signature_text=key)
        alias = Signature()
        alias.signature_text = key
        alias.parent = parentObj
        aliasObjs.append(alias)
    Signature.objects.bulk_create(list(aliasObjs))

    print(" ")
    print("Creating meta records")
    for i in df.index:
        printProgressBar(i, len(df.index), prefix='Progress: ', suffix='Complete', length=50)
        id = df['Signum'][i]
        foundLocation = df['Plats'][i];
        # convert double spaces to single
        id = id.replace("  ", " ")
        cleanId = normalizeId(id)

        # it is actually sufficient to check ('=' in id) only
        if (pd.isnull(foundLocation) and ('=' in id)) or (cleanId in manualAliases):
            continue

        lostCharPos = id.find('†');
        newReadingPos = id.find('$');
        recentPos = id.find('SENTIDA');

        inscr = Signature.objects.get(signature_text=cleanId)

        scrape = scrapes[cleanId]
        if scrape:
            foundCoordinates = scrape['found_lat_lon']
            newLocation = scrape['current_lat_lon']
        else:
            foundCoordinates = [0, 0]
            newLocation = [0, 0]
            
        #if len(foundCoordinates) == 2:
            #if (foundCoordinates[0] < 29 and foundCoordinates[1] > 40) or cleanId.find("By NT1984;32") != -1:
                ## we need to swap lat and lon
                #foundCoordinates = list(reversed(foundCoordinates))

        #newLocation = myGeo.parseRundataCoordinates(df['Nuv. koord.'][i])

        materialType = df['Materialtyp'][i]
        if not pd.isnull(materialType):
            try:
                materialType = MaterialType.objects.get(name=materialTypesMap[materialType])
            except MaterialType.DoesNotExist:
                materialType = MaterialType.objects.get(name='unknown')
        else:
            materialType = None

        # Create meta information object
        mi = MetaInformation();
        mi.found_location = foundLocation
        mi.parish = toText(df['Socken'][i])
        mi.district = toText(df['Härad'][i])
        mi.municipality = toText(df['Kommun'][i])
        mi.current_location = toText(df['Placering'][i])
        mi.latitude = foundCoordinates[0] if len(foundCoordinates) == 2 else 0
        mi.longitude = foundCoordinates[1] if len(foundCoordinates) == 2 else 0
        mi.original_site = toText(df['Urspr. plats?'][i])
        mi.present_latitude = newLocation[0] if len(newLocation) == 2 else 0
        mi.present_longitude = newLocation[1] if len(newLocation) == 2 else 0
        mi.parish_code = toText(df['Sockenkod/Fornlämningsnr.'][i])
        mi.rune_type = toText(df['Runtyper'][i])
        mi.dating = toText(df['Period/Datering'][i])
        mi.style = toText(df['Stilgruppering'][i])
        mi.carver = toText(df['Ristare'][i])
        mi.materialType = materialType
        mi.material = toText(df['Material'][i])
        mi.objectInfo = toText(df['Föremål'][i])
        mi.additional = toText(df['Övrigt'][i])
        mi.reference = toText(df['Referens'][i])
        mi.lost = lostCharPos != -1
        mi.new_reading = newReadingPos != -1
        mi.recent = recentPos != -1
        mi.signature = inscr
        metaObjs.append(mi)

    MetaInformation.objects.bulk_create(list(metaObjs))
    
    mi = MetaInformation.objects.get(pk=2)
    if not mi.new_reading:
        print("")
        print("Meta information is wrong. Will quit")
        return

    print(" ")
    print("Processing crosses")
    for i in df.index:
        printProgressBar(i, len(df.index), prefix='Progress: ', suffix='Complete', length=50)
        id = df['Signum'][i]
        foundLocation = df['Plats'][i];
        # convert double spaces to single
        id = id.replace("  ", " ")
        cleanId = normalizeId(id)

        if (cleanId.find("Ög Carlsson2012;19") != -1):
            # Incorrect cross form format is written in the DB. It should contain
            # two crosses. Overwrite DB value.
            df['Korsform'][i] = 'A1; ? ; 0 ; ?; 0; F2; 0 & A1; ?; 0; ?; E7; F3; 0'

        # it is actually sufficient to check ('=' in id) only
        if (pd.isnull(foundLocation) and ('=' in id)) or (cleanId in manualAliases):
            continue

        # handle crosses
        if not pd.isnull(df['Korsform'][i]):
            mi = MetaInformation.objects.get(signature__signature_text=cleanId)
            differentCrosses = df['Korsform'][i].split('&');
            for crossString in differentCrosses:
                crossIsValid = False
                crossGroupsList = crossString.strip().split(';')

                dbCross = Cross.objects.create(meta=mi)

                for crossGroup in crossGroupsList:
                    crossFormsInGroup = re.split(',|-', crossGroup)

                    for crossForm in crossFormsInGroup:
                        crossForm = crossForm.replace('[Klassificerad av redaktionen]', '')
                        crossForm = crossForm.strip()
                        # skip empty and invalid parts
                        if not crossForm:
                            continue

                        if (crossForm in manualCrossForms):
                            additionalGroups = crossForm.split(' ');
                            additionalGroups[0] = additionalGroups[0].strip()
                            additionalGroups[1] = additionalGroups[1].strip()
                            certain = True
                            if additionalGroups[0] != '0':
                                try:
                                    dbCrossForm = CrossForm.objects.get(name=additionalGroups[0])
                                except:
                                    dbCrossForm = CrossForm.objects.create(name=additionalGroups[0])
                                dbCrossDefinition = CrossDefinition(cross=dbCross, form=dbCrossForm, is_certain=certain)
                                dbCrossDefinition.save()

                            certain = False
                            additionalGroups[1] = additionalGroups[1].replace('?', '').replace(')', '').replace('(', '').strip()
                            try:
                                dbCrossForm = CrossForm.objects.get(name=additionalGroups[1])
                            except:
                                dbCrossForm = CrossForm.objects.create(name=additionalGroups[1])
                            dbCrossDefinition = CrossDefinition(cross=dbCross, form=dbCrossForm, is_certain=certain)
                            dbCrossDefinition.save()
                            crossIsValid = True
                            continue # move on to the next crossForm in crossFormsInGroup

                        if crossForm == '?' or crossForm == '0':
                            # no information whatsoever
                            continue;

                        certain = True
                        if crossForm.find('?') != -1:
                            certain = False
                            crossForm = crossForm.replace('?', '')

                        crossForm = crossForm.replace(')', '').replace('(', '').strip()

                        try:
                            dbCrossForm = CrossForm.objects.get(name=crossForm)
                        except:
                            dbCrossForm = CrossForm.objects.create(name=crossForm)

                        dbCrossDefinition = CrossDefinition(cross=dbCross, form=dbCrossForm, is_certain=certain)
                        dbCrossDefinition.save()
                        crossIsValid = True

                if not crossIsValid:
                    dbCross.delete()

    print('')
    print('Done')

if __name__ == "__main__":
    main()