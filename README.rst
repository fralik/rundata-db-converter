###############################
Rundata-net database conversion
###############################

This is the code used to convert original Rundata database to Rundata-net
format. Since we only need to run it once, the code is unpolished and might
be hacky in some places.

If you run from the very very scratch, you have to:

#. Run ``scrapInskrifter.py`` to get coordinates and links to images in ``scrappedSignatures.p`` file.
#. Run ``importMeta.py``.
#. Run ``importImages.py``.
#. Run ``dbFilesImport.py``.

The client code relies on the presence of these views:

#. ``all_data``::

    SELECT signatures.signature_text, meta_information.*,
    ifnull(t1.num_names, 0) as num_names, ifnull(t2.num_crosses, 0) as num_crosses,
    material_types.name AS "material_type",
    normalisation_norse.value AS normalisation_norse,
    normalisation_norse.search_value AS normalisation_search_norse,
    normalisation_scandinavian.value AS normalisation_scandinavian,
    normalisation_scandinavian.search_value AS normalisation_search_scandinavian,
    translation_english.value AS english_translation,
    transliterated_text.value AS transliteration,
    transliterated_text.search_value AS search_transliteration
    FROM meta_information
    INNER JOIN signatures ON (meta_information.signature_id = signatures.id)
    INNER JOIN normalisation_norse ON (normalisation_norse.signature_id = meta_information.signature_id)
    INNER JOIN normalisation_scandinavian ON (normalisation_scandinavian.signature_id = signatures.id)
    INNER JOIN translation_english ON (translation_english.signature_id = signatures.id)
    INNER JOIN transliterated_text ON (transliterated_text.signature_id = signatures.id)
    LEFT OUTER JOIN (SELECT signature_id, count(name_id) AS num_names from "runes_nameusage" GROUP BY signature_id) AS t1 ON (t1.signature_id = meta_information.signature_id)
    LEFT OUTER JOIN (SELECT meta_id, count(id) as num_crosses FROM "crosses" GROUP BY crosses.meta_id) AS t2 on (t2.meta_id = meta_information.id)
    LEFT OUTER JOIN material_types ON (material_types.id = meta_information.materialType_id)
    GROUP by meta_information.id

#. ``signatures_with_children``::

    select parent_id from signatures where parent_id is not null group by parent_id

#. ``root_signatures``::

    select signatures.id as id from signatures where signatures.parent_id IS NULL

List of manually modified inscriptions:

#. G 341. Set search_value in transliteration to ``... ... tan botuiþr lit kiira ...``.

*************************
Addition in 2024
*************************

There were these additions in 2024:

* Current location coordinates were added to the database.
* Swedish translation. See `addSwedishTranslation.py`.
* Dates (years) added via `dbAddYears.py`.


*************************
Development
*************************

Use the same python version as on rundata. Originally it was using python 3.5 (year 2018). We switched to python 3.12
in 2023/2024.